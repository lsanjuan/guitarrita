#lang scribble/manual

@title{@tt{guitarrita}, the CLI}

@section{First steps}

The @tt{guitarrita} script should work fine on Linux, MacOSX and the WSL
(Windows subsystem for Linux). There is no plan to release a native Windows
script.

For a first grasp on @tt{guitarrita} type this:

@verbatim[#:indent 2]{
 guitarrita --help
}

If that's enough for you, you are done. Forget what follows and explore things
for yourself. Otherwise, keep reading.

@section{Command line options}

From here on you will find the at-slow-pace-for-mortals-explanation of what you
can do with @tt{guitarrita}.

In the next section about options, option flags with arguments for them
are written down. The command itself requires the input file path name,
that is omitted for brevity. For fully funcional commands jump into
the @italic{Examples of use} subsection below.

@subsection{Finger marks style}

In chord diagrams you can see two main styles of marking fingers.
The first one, probably the most prevalent, is to draw a black dot to notate 
where to place your finger tips. It is, though, more convenient to notate
exactly which finger you have to use, especially in pedagogical contexts.
@tt{guitarrita} provides support for both styles.

@verbatim[#:indent 2]{
 guitarrita --with-dots
 guitarrita -d
}

uses dots, while

@verbatim[#:indent 2]{
 guitarrita --with-numbers
 guitarrita -n
}

prints fingering numbers instead.

The default style is @code{--with-numbers}.


@subsection{Action modes}

You may want to just silently generate image files for the text diagrams,
or you may prefer to see the resulting image on your image viewer program
and later decide whether to save it or not. Moreover, if you have in
mind to use or process the result in some further way, you may wish 
for a pure @italic{xml} description of the result or, even, a bunch of
bytes.

@tt{guitarrita} supports the three options:

@verbatim[#:indent 2]{
 guitarrita --view
 guitarrita -w
}

will open the resulting image and display it in your default image viewer.

@verbatim[#:indent 2]{
 guitarrita --output <outfile>
 guitarrita -o <outfile>
}

saves an image as @tt{<outfile>}.

Finally,

@verbatim[#:indent 2]{
 guitarrita --raw
 guitarrita -r
}

displays on the terminal the raw bytes encoding the diagram. If the
image format is @tt{svg}, you will get the xml utf-8 string.

The default action mode is @code{--view}.

@subsection{Output image format}

As for the output format, when an image is saved or viewed, you probably expect
multiple choices. @tt{guitarrita} supports two modern outputs,
@italic{svg} (the default output format) and @italic{png} via:

@verbatim[#:indent 2]{
 guitarrita --png
 guitarrita --svg
}

respectively.

The default output format is @code{--svg}.

@subsection{To name or not to name}

In didactic contexts it might be convenient to hide the chord name so that
your students guess the name from the diagram. For this reason an option to
hide the chord name is given:

@verbatim[#:indent 2]{
 guitarrita --hide-name
 guitarrita -N
}

@subsection{Lovely colors}

In modern times it is likely that you use these diagrams on the web or over
some electronic medium. Color is nowadays a must-have. Some support for colors
is given via these flags:

@verbatim[#:indent 2]{
 guitarrita -c <palette>
 guitarrita --color <palette>
}

where @tt{palette} is, currently, one of @tt{kid} or @tt{adult}.

Without that flag you'll still get the bored but solid traditional black and
white diagram.

@section{Examples of use}

Display an @italic{svg} diagram from @tt{<infile>}

@verbatim[#:indent 2]{
 guitarrita <infile> 
}

Save an @italic{svg} image as @tt{outfile} from @tt{<infile>} 

@verbatim[#:indent 2]{
 guitarrita -o <outfile> <infile>
}

Same as above but use dots instead of numbers as finger marks

@verbatim[#:indent 2]{
 guitarrita -do <outfile> <infile>
}

As above but colorized with the @tt{kid} palette

@verbatim[#:indent 2]{
 guitarrita -o <outfile> -dc kid <infile>
}

The same, but printing the @italic{xml} output to the console, instead of
saving the output.

@verbatim[#:indent 2]{
 guitarrita -rdc kid <infile>
}

Another way to save the result as @tt{<outfile>} with redirection

@verbatim[#:indent 2]{
 guitarrita -rdc kid <infile> > <outfile>.svg
}

Passing a string instead of a file to @tt{guitarrita}. Nice for quick
commands

@verbatim[#:indent 2]{
 guitarrita <(echo -e "tab\n1\n------")
}

With a bit of shell you can process multiple files in a single command

@verbatim[#:indent 2]{
 for d in $(ls <tabtexts-dir>/*.txt); do
   guitarrita -o <output-dir>/"$(basename "$d" ".txt")".svg "$d"
 done
}

A couple of more cryptic commands that show the flexibility of CLI apps.

The first command opens the xml description of the diagram in the powerful 
vim editor; the last one converts the image into a pdf file:

@verbatim[#:indent 2]{
 guitarrita -r <infile> | vim -c 'set syntax=xml' -
 inkscape --without-gui --export--pdf <infile>.pdf <(guitarrita -r <infile>)
}

And, sure, you can use @tt{guitarrita} in more complex scripts. Look into
@tt{examples/chords2html.sh} for an illustration. 

