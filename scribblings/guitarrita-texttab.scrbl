#lang scribble/manual

@title{@tt{texttab} specification}

@section{@tt{texttab} lines}

This an informal specification of the format that @tt{guitarrita}
expects for input files to have. I call it @tt{texttab}.

A @tt{texttab} file or string consists of the following lines:

@itemlist[
 #:style 'ordered
 @item{The name of the chord.}
 @item{The initial fret as an arabic number.} 
 @item{Six string marks, where an string mark is one of: @tt{O} (open string), 
  @tt{R} (root), @tt{X} (muted string), or @tt{-} (no mark). Lowercase letters
  are also valid.}
 @item{The rest of the lines represent the fretboard. Each line represents a 
   fret beginning from the first fret. Each fret consists of six finger marks,
   one per guitar string, from the 6th to the 1st one. Finger marks are notated
   with numbers, from 1 to 4, as customary in guitar fingerings.}
 ]

The fretboard lines are technically optional. A file with only the first three
lines is allowed.

@section{Examples}

This the @italic{C} chord:

@verbatim[#:indent 2]{
 C
 1
 XR-O--
 ----1-
 --2---
 -3----
}

and this one the @italic{B7} chord:

@verbatim[#:indent 2]{
 B7
 2
 xr----
 -11111
 ------
 --3-4-
}

