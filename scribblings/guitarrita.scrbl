#lang scribble/manual

@title{Drawing guitar chord diagrams with Racket}
@author[(@author+email "Luis Sanjuán" "luisj dot sanjuan at gmail dot com")]

@tt{guitarrita} is a CLI program to produce diagrams of guitar chords from
descriptions of those chords written in a simple and natural plain text format.

Therefore, @tt{guitarrita} consists of two parts: the command line tool itself
and the library code on which it relies.
@margin-note{Note that the tool and the library
are bundled together without further distinction. It would be better to
distribute them as at least two different packages and to disentangle
 the library code from the user code. Feel free to do so
if @tt{guitarrita} attracts more people than I'm expecting. In the meantime
or in case it still remains a niche tool for few musicians, a single
             package is just easier to google for and to work with.}

The first section of this document introduces the necessary domain knowledge
and tell a bit about the motivation behind @tt{guitarrita}. The second section
quickly presents the command line options. The third section documents
the texttab specification.

Go right to the second section if you just want to use @tt{guitarrita}
and you already know what this stuff is all about after a quick reading of the
third section. Refer to the first section to refresh your knowledge about
guitar chord diagrams and to know what you can expect from @tt{guitarrita}.

@include-section["guitarrita-intro.scrbl"]
@include-section["guitarrita-cli.scrbl"]
@include-section["guitarrita-texttab.scrbl"]
