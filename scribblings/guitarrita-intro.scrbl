#lang scribble/manual

@require[scribble/core
         scribble/html-properties
         scriblib/figure
         racket/runtime-path]

@(define-runtime-path resized-img-css "resized-img.css")
@(define-runtime-path sanz-jpg "img/sanz-instruccion-diagrams.jpg")
@(define-runtime-path till-we-meet-jpg "img/till-we-meet-again-diagrams.jpg")
@(define-runtime-path guit-def-svg "img/c7-with-guitarrita.svg")
@(define-runtime-path guit-kid-svg "img/c7-with-guitarrita-kid.svg")
@(define-runtime-path guit-adult-svg "img/c7-with-guitarrita-adult.svg")
 
@(define resized-img
   (make-style "ResizedImg"
               (list (make-css-addition resized-img-css))))

@title{Guitar chords with guitarrita}

@section{Guitar chords diagrams}

Guitar chord diagrams are as old as printed music for plucked instruments.
See, for instance, this plate from an important 17th. century manuscript:

@margin-note{
 @italic{source}: Oberlin Conservatory  Library Special Collections
 @url{https://archive.org/details/instrucciondemus0000sanz/page/95}
}
@figure[
 "diagram-17th-century"
 @elem{Gaspar Sanz,
  @italic{Instrucción de música sobre la guitarra española} (1697)}
 @image[sanz-jpg #:style resized-img]
 ]

Each diagram there represents a guitar left-hand position. The representation
is self-explanatory, for it is a realistic drawing of actual fingerings, one
per chord. Chord names are given as a single uppercase letter above the
corresponding diagram.

Modern incarnations are more abstract. Look at the following sheet, published
in the second decade of the 20th. century:

@margin-note{
 @italic{source}: UNC Sheet Music Collection.
 @url{https://archive.org/details/tillwemeetagain00whit_0/page/n1}}
@figure[
 "diagram-early-20th-century"
 @elem{@italic{Till we meet again} (1918)}
 @image[till-we-meet-jpg #:style resized-img]
 ]

Diagrams are represented as small guitar outlines above the staves. Chord names
appear above diagrams as before. The fretboard is still rendered almost
realistically, but the orientation is now vertical. The main difference lies
in the representation of left-hand fingers. Instead of real fingers we have
black dots, one per finger. In addition, certain strings get marks. See, for
instance, the small @tt{o} marking the first string in the @italic{C7} chord.
It means that the string has to be plucked despite no finger is pressing
down any fret. Finally, the curve joining some strings in certain positions
represents a special technique called @italic{barre} by which multiple strings
are pressed down with the same finger, typically the index.

The latter example with a few more additions and variants has become the
standard way of representing guitar chords with diagrams.

In contemporary scores you may see an "x" mark for strings that shouldn't
be played and, even, a mark for the root of the chord, normally in bass
position. Also, finger marks can be numbers instead of dots, where each number,
from 1 to 4, represents a particular finger.

This is the same @italic{C7} chord that @tt{guitarrita} produces rendered
with different styles:

@figure["default-guitarrita-diagram"
        @elem{@italic{C7} @tt{guitarrita} diagram in multiple styles}
        @image[guit-def-svg #:style resized-img]
        @image[guit-kid-svg #:style resized-img]
        @image[guit-adult-svg #:style resized-img]]

@section{The motivation of guitarrita}

During the COVID-19 strict quarantine all college schools were closed.
Professors and teachers all around the world had to prepare
electronic materials of any kind at very fast pace. In face-to-face
classes usual activities like drawing a chord diagram on the
blackboard became impossible. We needed tools to keep doing the job.

After some research into available options I decided to implement
my own tool, one with greater flexibility than that I was able to find out
there.

In particular, I wanted to tweak string marks according to my own
conventions, change colors at will depending on the student's level,
hide the chord name in assignments, etc. On the other hand, scalable vector
graphics were a must-have because the information had to be communicated over
the web. Finally, I always choose tools suitable to scripting for
batch processing or for any other unanticipated task.

What you get here is then the result of organizing, clearing and politely
putting ---as much as my skill and time permits--- a bunch of codes that I
wrote a couple of months ago with the mentioned motivation in mind.

So you should expect these features (and drawbacks).

@itemlist[
 @item{The user interface of @tt{guitarrita} is the command line. No GUI at this
  moment.}
 @item{The expected input is a text file that follows an informal specification
  for representing guitar chord diagrams. This specification can be learned in
  a minute.}
 @item{The default output format is @italic{svg}, basic @italic{png} support is also provided
  and more formats can be added if the need arise.}
 @item{Color support for all parts of the diagram is implemented, and it is
  quite easy, if you submit proposals, to add more custom palettes. Currently,
  two palettes are provided.}]

For the curious, @italic{guitarrita} is a Spanish word, the diminutive of @italic{guitarra}.
Younger students like this term. It seems to be quite expressive to them.

