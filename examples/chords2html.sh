#!/usr/bin/env bash

# Create a webpage of chord diagrams
# Assume: You run the script from this directory 
# Usage:
#     chords2html <chord-dir>

CHORD_DIR="$1"
TEMPLATE="template.html"

if [ ! -e img ]; then
    mkdir img
fi

# Generate svg images and html img tags for them
for d in $(ls "$CHORD_DIR"/*.txt); do
    chord_name=$(basename "$d" .txt | cut -d'_' -f2)
    guitarrita -rc adult "$d" > img/"${chord_name}".svg
    echo "<img title=\""$chord_name"\" src=\"img/"$chord_name".svg\">" >> tags 
done

# Fill the template and clear
sed '/<body>/r tags' "$TEMPLATE" > guitar_chords.html 
rm tags

