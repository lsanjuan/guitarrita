# Guitarrita: Drawing guitar chord diagrams from the CLI

## Description

`guitarita` is a command line tool to draw guitar chord diagrams
from files written in a plain text format.

## Requirements

- OS: Linux, MacOSX, WSL over Windows
- Racket: <https://racket-lang.org/download/>

## Installation

    raco pkg install --clone guitarrita https://gitlab.com/lsanjuan/guitarrita.git

If executing `guitarrita` prints `command not found`, type this to know 
where `guitarrita` is:

    find / -name guitarrita 2>/dev/null | grep bin

Let's call the result `<guitarrita-path>`.

You can now execute `guitarrita` by typing `<guitarrita-path>`.

This is inconvenient, and you may want to create a symbolic link or add
the directory of Racket user executables to your PATH.

If you choose the first option, type this:

    echo "alias guitarrita=\"<guitarrita-path>\"" >> ~/.bashrc
    source ~/.bashrc

If you prefer the second option, type this instead:

    echo "PATH=$(dirname <guitarrita-path>):\$PATH" >> ~/.profile
    source ~/.profile

## Usage

    guitarrita --help

gives you a quick help about `guitarrita`.

For more information look into the package documentation:

    raco docs 'texttab spec'

explains the rules a text file (or string) for guitar chords must obey.

    raco docs 'guitarrita cli'

documents all available options in more detail.

    raco docs 'drawing chords'

contains background information about guitar chord diagrams.



