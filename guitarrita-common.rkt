#lang racket/base

;; =============================================================================
;;
;; Core definitions
;;
;; =============================================================================

(require racket/contract)

(provide (contract-out
          [struct tab ([name string?]
                         [initial-fret fretnum?]
                         [string-marks string-marks?]
                         [fingering fingering?])]
          
          [strings-in-fret exact-positive-integer?]
          
          [tab=? (-> tab? tab? boolean?)]

          [string-mark? predicate/c]
          [string-marks? predicate/c]
          [fretnum? predicate/c]
          [finger-mark? predicate/c]
          [fret? predicate/c]
          [fingering? predicate/c]

          [finger-mark-style? predicate/c]
          [action-mode? predicate/c]
          [tab-diagram-format? predicate/c]))

(struct tab [name initial-fret string-marks fingering])

(define (tab=? c1 c2)
  (and (string=? (tab-name c1) (tab-name c2))
       (= (tab-initial-fret c1) (tab-initial-fret c2))
       (equal? (tab-string-marks c1) (tab-string-marks c2))
       (equal? (tab-fingering c1) (tab-fingering c2))))

(define strings-in-fret 6)

(define action-mode? (or/c 'display 'save 'view))

(define string-mark? (or/c 'mute 'none 'open 'root))

(define (string-marks? v)
  (and (list? v)
       (= (length v) strings-in-fret)
       (andmap string-mark? v)))

(define fretnum? (integer-in 1 12))

(define finger-mark? (or/c 1 2 3 4 #f))

(define finger-mark-style? (or/c 'barre 'dot 'number))

(define (fret? v)
  (and (list? v)
       (= (length v) strings-in-fret)
       (andmap finger-mark? v)))

(define (fingering? v) (and (list? v) (andmap fret? v)))

(define tab-diagram-format? (or/c 'png 'svg))




